let collection = [];

// Write the queue functions below.

//   Print queue elements.
function print(item){
    return collection;
  }
  
//  Enqueue a new element.
function enqueue(item){
    if(collection.length === 0) {
        collection[0] = item;
    }else{
        collection[collection.length] = item;
    }
    return collection;
  }

// Dequeue the first element.
function dequeue(item) {
    let deconstructCollection = [];
    for(let i = 0; i < collection.length - 1; i++){
        deconstructCollection[i] = collection[i+1];
    }
    return collection = [...new Set(deconstructCollection)]
  }

// Get first element.
function front(item) {
    return collection[0];
}

// Get queue size.
function size(item) {
    return collection.length;
}

//  Check if queue is not empty.
function isEmpty(item) {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};